
## dark-crystal-rust

Workspace containing the [dark-crystal-key-backup-rust](https://gitlab.com/dark-crystal-rust/dark-crystal-key-backup-rust) and [dark-crystal-secret-sharing-rust](https://gitlab.com/dark-crystal-rust/dark-crystal-secret-sharing-rust) crates, as well as a simple command line interface to create shares of a secret and encrypt them to a given set of custodian's public keys.

For usage information type `cargo run --bin cli -- help`
