use clap::{AppSettings, Parser, Subcommand};
use colored::*;
use dark_crystal_key_backup_rust::{combine_authenticated, share_and_encrypt};
use hex;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Create a share set
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Share {
        /// The secret
        #[clap(short, long)]
        secret: String,

        /// Public encryption keys of recovery partners
        #[clap(short, long)]
        pk: Vec<String>,
    },
    /// Recover a secret
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Combine {
        shares: Vec<String>,
        #[clap(short, long)]
        ciphertext: String,
    },
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ShareOutput {
    ephemeral_public_key: String,
    ciphertext: String,
    shares: Vec<String>,
}

fn main() {
    let args = Cli::parse();

    match &args.command {
        Commands::Share { secret, pk } => {
            let mut public_keys: Vec<[u8; 32]> = Vec::new();
            for pk in pk.iter() {
                public_keys.push(hex::decode(&pk).unwrap().try_into().unwrap());
            }

            let secret = hex::decode(&secret).unwrap();
            let encrypted_share_set = share_and_encrypt(public_keys, secret, 2).unwrap();
            let mut total_bytes = 0;

            let share_output = ShareOutput {
                ephemeral_public_key: hex::encode(encrypted_share_set.eph_public_key.as_bytes()),
                ciphertext: hex::encode(&encrypted_share_set.ciphertext),
                shares: encrypted_share_set
                    .encrypted_shares
                    .iter()
                    .map(|share| hex::encode(share))
                    .collect(),
            };

            println!(
                "\n{} {} ({} bytes)",
                "Ephemeral public key:".green(),
                share_output.ephemeral_public_key,
                encrypted_share_set.eph_public_key.as_bytes().len()
            );
            total_bytes += encrypted_share_set.eph_public_key.as_bytes().len();
            println!(
                "\n{} {} ({} bytes)",
                "Ciphertext:".green(),
                share_output.ciphertext,
                encrypted_share_set.ciphertext.len()
            );
            total_bytes += encrypted_share_set.ciphertext.len();
            println!("\n{}", "Shares:".green());
            for share in share_output.shares.iter() {
                println!("{} ({} bytes)", share, share.len() / 2);
                total_bytes += share.len() / 2;
            }

            println!("\n{} bytes in total\n", total_bytes);
            println!("{}", "As JSON:".green());
            println!("{}", serde_json::to_string_pretty(&share_output).unwrap());
        }
        Commands::Combine { shares, ciphertext } => {
            let mut shares_bytes: Vec<Vec<u8>> = Vec::new();
            for share in shares.iter() {
                shares_bytes.push(hex::decode(&share).unwrap());
            }

            let recovered_secret =
                combine_authenticated(shares_bytes, hex::decode(ciphertext).unwrap()).unwrap();
            println!("Recovered: {}", hex::encode(recovered_secret));
        }
    }
}
